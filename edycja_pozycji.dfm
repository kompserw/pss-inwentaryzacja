object frEdycjaPozycji: TfrEdycjaPozycji
  Left = 569
  Top = 307
  Width = 431
  Height = 162
  Caption = 'Edycja pozycji'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  ShowHint = True
  PixelsPerInch = 96
  TextHeight = 13
  object LabeledEdit1: TLabeledEdit
    Left = 16
    Top = 18
    Width = 385
    Height = 21
    EditLabel.Width = 69
    EditLabel.Height = 13
    EditLabel.Caption = 'Nazwa towaru'
    TabOrder = 0
  end
  object LabeledEdit2: TLabeledEdit
    Left = 16
    Top = 72
    Width = 113
    Height = 21
    EditLabel.Width = 77
    EditLabel.Height = 13
    EditLabel.Caption = 'Cena sprzeda'#380'y'
    TabOrder = 1
  end
  object LabeledEdit3: TLabeledEdit
    Left = 136
    Top = 72
    Width = 121
    Height = 21
    EditLabel.Width = 68
    EditLabel.Height = 13
    EditLabel.Caption = 'Ilo'#347#263' lub waga'
    TabOrder = 2
  end
  object JvImgBtn1: TJvImgBtn
    Left = 280
    Top = 44
    Width = 121
    Height = 41
    Caption = 'Zapisz zmiany'
    TabOrder = 3
    OnClick = JvImgBtn1Click
    Flat = True
    Images = frGlowny.ImageList1
    ImageIndex = 7
  end
  object JvImgBtn2: TJvImgBtn
    Left = 280
    Top = 87
    Width = 121
    Height = 33
    Caption = 'Anuluj zmiany'
    TabOrder = 4
    OnClick = JvImgBtn2Click
    Flat = True
    Images = frGlowny.ImageList1
    ImageIndex = 12
  end
end
