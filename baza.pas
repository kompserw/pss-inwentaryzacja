unit baza;

interface

uses
  SysUtils, Classes, DB, JvCsvData, IBDatabase, IBCustomDataSet, IBQuery,
  IBTable;

type
  Tdm = class(TDataModule)
    dsCSV: TDataSource;
    csv: TJvCsvDataSet;
    serwer: TIBDatabase;
    Transakcje: TIBTransaction;
    ibqZapiszCSV: TIBQuery;
    csv_towary: TJvCsvDataSet;
    dsCSV_towary: TDataSource;
    ibqZapiszTowary: TIBQuery;
    ibqZapiszArkusz: TIBQuery;
    ibqNumery: TIBQuery;
    ibqTowary: TIBQuery;
    ibqPokazArkusz: TIBQuery;
    dsPokazArkusz: TDataSource;
    ibqTMP: TIBQuery;
    dsTMP_Pokaz: TDataSource;
    ibtTMP_Pokaz: TIBTable;
    ibqSklepy: TIBQuery;
    dsSklepy: TDataSource;
    dsTowary: TDataSource;
    ibtSklepy: TIBTable;
    dsSklepCombo: TDataSource;
    ibqPokazPozycje: TIBQuery;
    dsPokazPozycje: TDataSource;
    ibqPokazArkusze: TIBQuery;
    dsPokazArkusze: TDataSource;
    ibqSumujPozycje: TIBQuery;
    procedure ZapiszCSV(numer,sklep: Integer;kod_kreskowy: String;cena,ilosc: Real);
    procedure ZapiszCSV_Towary(sklep: Integer;nazwa,kod_kreskowy: String;cena: Real);
    procedure ZwiekszNr(numer: Integer);
    procedure ZapiszArkusz(nr_towaru,nr_import,nr_sklep: Integer;ilosc,cena,wartosc: Real; nazwa,typ,dok_pierwotny: String);
    procedure PokazArkusz(nr_import: Integer);
    procedure PokazPozycje(nr_import: Integer);overload;
    procedure PokazArkusze;overload;
    procedure DodajSklep(nazwa,ulica,miasto,kod_p: String);
    procedure PokazSklepy;overload;
    procedure PokazTowary(sklep: Integer);
    procedure ZapiszTMP(numer,sklep,pozycja_pliku: Integer; ilosc,cena,wartosc: Real; kod_kreskowy: String);
    procedure PokazTMP;
    procedure KopiujTMP(x: Integer;typ,dok_pierwotny: String);
    procedure KasujTMP;
    procedure KasujTowary(sklep: Integer);overload;
    procedure KasujBazy;
    procedure SzukajTowar(szukaj_po,sklep: Integer;szukaj: String);
    function PodajNr(): Integer;
    function WyszukajTowary(kod_kreskowy: String; sklep: Integer): Integer;
    function NrTowaru(kod_kreskowy: String): Integer;
    function NazwaTowaru(nr_towar: Integer): String;
    function NrSklepu(nazwa: String): Integer;
    function SprawdzSklep(nazwa: String): Boolean;
    function SumujPozycje(nr_importu: Integer): Real;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure PokazSklepy(nr_sklepu: Integer);overload;
    procedure PokazSklepy(nazwa: String);overload;
    procedure PokazPozycje(nr_import,sortowanie: Integer);overload;
    procedure KasujTowary;overload;
    procedure PokazArkusze(DataOd,DataDo: TDateTime);overload;
    procedure PokazArkusze(NrOd,NrDo: Integer);overload;
    procedure PoprawArkusz(nr_towaru,nr_import,nr_sklep: Integer;ilosc,cena,wartosc: Real; nazwa,typ,dok_pierwotny: String);
    procedure KasujPozycjeArkusz(nr_towaru,nr_import,nr_sklep: Integer);
  end;

var
  dm: Tdm;

implementation

uses glowny;

{$R *.dfm}

{ Tdm }

{ Tdm }

{ Tdm }

procedure Tdm.DodajSklep(nazwa, ulica, miasto, kod_p: String);
begin
if not dm.Transakcje.InTransaction then dm.Transakcje.StartTransaction;
with dm.ibqSklepy do
     begin
          Close;
          SQL.Clear;
          SQL.Add('INSERT INTO SKLEPY(NAZWA,ULICA,MIASTO,KOD_P)');
          SQL.Add('VALUES(:A,:B,:C,:D)');
          Unprepare;
              ParamByName('A').AsString := nazwa;
              ParamByName('B').AsString := ulica;
              ParamByName('C').AsString := miasto;
              ParamByName('D').AsString := kod_p;
          Open;
     end;
if dm.Transakcje.InTransaction then dm.Transakcje.Commit;
dm.PokazSklepy;
end;

procedure Tdm.KasujBazy;
begin
if not dm.Transakcje.InTransaction then dm.Transakcje.StartTransaction;
with dm.ibqTMP do
     begin
          Close;
          SQL.Clear;
          SQL.Add('DELETE FROM IMPORT');
          Open;
     end;
with dm.ibqTMP do
     begin
          Close;
          SQL.Clear;
          SQL.Add('DELETE FROM ARKUSZ');
          Open;
     end;
if dm.Transakcje.InTransaction then dm.Transakcje.Commit;
end;

procedure Tdm.KasujTMP;
begin
if not dm.Transakcje.InTransaction then dm.Transakcje.StartTransaction;
with dm.ibqTMP do
     begin
          Close;
          SQL.Clear;
          SQL.Add('DELETE FROM TMP');
          Open;
     end;
if dm.Transakcje.InTransaction then dm.Transakcje.Commit;
end;

procedure Tdm.KasujTowary(sklep: Integer);
begin
if not dm.Transakcje.InTransaction then dm.Transakcje.StartTransaction;
with dm.ibqTowary do
     begin
          Close;
          SQL.Clear;
          SQL.Add('DELETE FROM TOWARY');
          SQL.Add('WHERE NR_SKLEPU = :A');
          Unprepare;
              ParamByName('A').AsInteger := sklep;
          Prepare;
          Open;
     end;
if dm.Transakcje.InTransaction then dm.Transakcje.Commit;
end;

procedure Tdm.KopiujTMP(x: Integer;typ,dok_pierwotny: String);
var
   nr_import,nr_sklep,ile,a: Integer;
   towar,kod_kreskowy: String;
   data_arkusza: TDateTime;
   ilosc,cena,wartosc: Real;
begin
dm.PokazTMP;
dm.ibqTMP.First;
a := 1;
while a <= x do
      with dm.ibqTMP,frGlowny.Siatka do
           begin
                nr_import := StrToInt(Cells[1,a]);
                nr_sklep := StrToInt(Cells[2,a]);
                towar := Cells[3,a];
                kod_kreskowy := Cells[4,a];
                data_arkusza := now();
                ilosc := StrToFloat(Cells[5,a]);
                cena := StrToFloat(Cells[6,a]);
                wartosc := StrToFloat(Cells[7,a]);
                dm.ZapiszCSV_Towary(nr_sklep,towar,kod_kreskowy,cena);
                dm.ZapiszArkusz(dm.NrTowaru(kod_kreskowy),nr_import,nr_sklep,ilosc,cena,wartosc,towar,typ,dok_pierwotny);
                a := a + 1;
           end;
dm.KasujTMP;
end;

function Tdm.NazwaTowaru(nr_towar: Integer): String;
begin
with dm.ibqTowary do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT NAZWA FROM TOWARY');
          SQL.Add('WHERE NR_TOWARY = :A');
          Unprepare;
              ParamByName('A').AsInteger := nr_towar;
          Prepare;
          Open;
          if FieldByName('NAZWA').IsNull then Result := 'Brak nazwy'
          else Result := FieldValues['NAZWA'];
     end;
end;

function Tdm.NrSklepu(nazwa: String): Integer;
begin
with dm.ibqSklepy do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT NR_SKLEPY FROM SKLEPY');
          SQL.Add('WHERE NAZWA = :A');
          Unprepare;
              ParamByName('A').AsString := nazwa;
          Prepare;
          Open;
          Result := FieldValues['NR_SKLEPY'];
     end;
end;

function Tdm.NrTowaru(kod_kreskowy: String): Integer;
begin
with dm.ibqTowary do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT NR_TOWARY FROM TOWARY');
          if kod_kreskowy <> '' then SQL.Add('WHERE KOD_KRESKOWY = :A');
          Unprepare;
              if kod_kreskowy <> '' then ParamByName('A').AsString := kod_kreskowy;
          Prepare;
          Open;
          if kod_kreskowy <> '' then Result := FieldValues['NR_TOWARY']
          else
              begin
                   Last;
                   Result := FieldValues['NR_TOWARY'] + 1;
              end;
     end;
end;

function Tdm.PodajNr: Integer;
begin
with dm.ibqNumery do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT NUMER FROM NUMERY');
          Open;
          Last;
          if FieldByName('NUMER').IsNull then Result := 0
          else Result := FieldValues['NUMER'];
     end
end;

procedure Tdm.PokazArkusz(nr_import: Integer);
begin
with dm.ibqPokazArkusz do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT A.NAZWA,A.ILOSC,A.CENA,A.WARTOSC,A.NAZWA_DOK_PIERW,A.AUTOMAT_RECZNY,A.DATA_ARKUSZA FROM ARKUSZ A,TOWARY B');
          SQL.Add('WHERE A.NR_TOWAR = B.NR_TOWARY AND A.NR_IMPORT = :A');
          SQL.Add('ORDER BY A.NAZWA');
          Unprepare;
             ParamByName('A').AsInteger := nr_import;
          Prepare;
          Open;
     end;
end;

procedure Tdm.PokazSklepy;
begin
with dm.ibqSklepy do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT * FROM SKLEPY');
          Open;
     end;
end;

procedure Tdm.PokazArkusze;
begin
with dm.ibqPokazArkusze do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT A.NR_IMPORT AS IMPORT,A.NAZWA_DOK_PIERW AS DOKUMENT,B.NAZWA,A.DATA_ARKUSZA AS DATA,SUM(A.WARTOSC) AS WARTOSC FROM ARKUSZ A,SKLEPY B');
          SQL.Add('WHERE A.NR_SKLEP = B.NR_SKLEPY');
          SQL.Add('GROUP BY A.NR_IMPORT,A.NAZWA_DOK_PIERW,B.NAZWA,A.DATA_ARKUSZA');
          SQL.Add('ORDER BY A.NR_IMPORT');
          Open;
     end;
end;

procedure Tdm.PokazSklepy(nr_sklepu: Integer);
begin
with dm.ibqSklepy do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT * FROM SKLEPY');
          SQL.Add('WHERE NR_SKLEPY = :A');
          Unprepare;
             ParamByName('A').AsInteger := nr_sklepu;
          Prepare;
          Open;
     end;
end;

procedure Tdm.PokazTMP;
begin
with dm.ibqTMP do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT * FROM TMP');
          Open;
     end;
end;

procedure Tdm.PokazTowary(sklep: Integer);
begin
with dm.ibqTowary do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT * FROM TOWARY A, SKLEPY B');
             if sklep > 0 then
                begin
                     SQL.Add('WHERE B.NR_SKLEPY = :A AND A.NR_SKLEPU = B.NR_SKLEPY');
                     Unprepare;
                        ParamByName('A').AsInteger := sklep;
                     Prepare;
                end
             else
             SQL.Add('WHERE A.NR_SKLEPU = B.NR_SKLEPY');
          Open;
     end;
end;

function Tdm.SprawdzSklep(nazwa: String): Boolean;
begin
with dm.ibqSklepy do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT NR_SKLEPY FROM SKLEPY');
          SQL.Add('WHERE NAZWA = :A');
          Unprepare;
              ParamByName('A').AsString := nazwa;
          Prepare;
          Open;
          if FieldByName('NR_SKLEPY').IsNull then Result := False
             else Result := True;
     end;
end;

procedure Tdm.SzukajTowar(szukaj_po,sklep: Integer; szukaj: String);
begin
with dm.ibqTowary do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT NAZWA,KOD_KRESKOWY,NR_TOWARY FROM TOWARY');
          case szukaj_po of
               0: ;
               1: SQL.Add('WHERE NAZWA LIKE :A');
               2: SQL.Add('WHERE KOD_KRESKOWY LIKE :A');
          end;
          Unprepare;
             ParamByName('A').AsString := '%' + szukaj + '%';
             //ParamByName('B').AsInteger := sklep;
          Prepare;
          Open;
     end;
frGlowny.UstawSiatkeReczny;
end;

function Tdm.WyszukajTowary(kod_kreskowy: String;sklep: Integer): Integer;
begin
with dm.ibqTowary do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT NR_TOWARY FROM TOWARY');
          SQL.Add('WHERE KOD_KRESKOWY = :A AND NR_SKLEPU = :B');
          Unprepare;
             ParamByName('A').AsString := kod_kreskowy;
             ParamByName('B').AsInteger := sklep;
          Prepare;
          Open;
          if FieldByName('NR_TOWARY').IsNull then Result := 0
          else
              Result := FieldValues['NR_TOWARY'];
     end;
end;

procedure Tdm.ZapiszArkusz(nr_towaru, nr_import, nr_sklep: Integer;
  ilosc,cena,wartosc: Real; nazwa,typ,dok_pierwotny: String);
begin
if not dm.Transakcje.InTransaction then dm.Transakcje.StartTransaction;
with dm.ibqZapiszArkusz do
     begin
          Close;
          SQL.Clear;
          SQL.Add('INSERT INTO ARKUSZ(NR_TOWAR,NAZWA,NR_IMPORT,NR_SKLEP,ILOSC,CENA,WARTOSC,NAZWA_DOK_PIERW,AUTOMAT_RECZNY)');
          SQL.Add('VALUES(:A,:G,:B,:C,:D,:E,:F,:H,:I)');
          Unprepare;
             ParamByName('A').AsInteger := nr_towaru;
             ParamByName('G').AsString := nazwa;
             ParamByName('B').AsInteger := nr_import;
             ParamByName('C').AsInteger := nr_sklep;
             ParamByName('D').AsFloat := ilosc;
             ParamByName('E').AsFloat := cena;
             ParamByName('F').AsFloat := wartosc;
             ParamByName('H').AsString := dok_pierwotny;
             ParamByName('I').AsString := typ;
          Prepare;
          Open;
     end;
if dm.Transakcje.InTransaction then dm.Transakcje.Commit;
end;

procedure Tdm.ZapiszCSV(numer,sklep: Integer; kod_kreskowy: String; cena,
  ilosc: Real);
begin
if not dm.Transakcje.InTransaction then dm.Transakcje.StartTransaction;
with dm.ibqZapiszCSV do
     begin
          Close;
          SQL.Clear;
          SQL.Add('INSERT INTO IMPORT(NR_SKLEP,NUMER,KOD_KRESKOWY,CENA,ILOSC)');
          SQL.Add('VALUES(:A,:F,:B,:C,:D)');
          Unprepare;
             ParamByName('A').AsInteger := sklep;
             ParamByName('F').AsInteger := numer;
             ParamByName('B').AsString := kod_kreskowy;
             ParamByName('C').AsFloat := cena;
             ParamByName('D').AsFloat := ilosc;
          Prepare;
          Open;
     end;
if dm.Transakcje.InTransaction then dm.Transakcje.Commit;
end;

procedure Tdm.ZapiszCSV_Towary(sklep: Integer; nazwa, kod_kreskowy: String;
  cena: Real);
begin
if not dm.Transakcje.InTransaction then dm.Transakcje.StartTransaction;
with dm.ibqZapiszTowary do
     begin
          Close;
          SQL.Clear;
          SQL.Add('INSERT INTO TOWARY(NR_SKLEPU,NAZWA,KOD_KRESKOWY,CENA_SPRZEDAZY,CENA_ZAKUPU)');
          SQL.Add('VALUES(:A,:B,:C,:D,:E)');
          Unprepare;
             ParamByName('A').AsInteger := sklep;
             ParamByName('B').AsString := nazwa;
             ParamByName('C').AsString := kod_kreskowy;
             ParamByName('D').AsFloat := cena;
             ParamByName('E').AsFloat := cena;
          Prepare;
          Open;
     end;
if dm.Transakcje.InTransaction then dm.Transakcje.Commit;
end;

procedure Tdm.ZapiszTMP(numer,sklep,pozycja_pliku: Integer; ilosc,cena,wartosc: Real; kod_kreskowy: String);
begin
with dm.ibqTMP do
     begin
          Close;
          SQL.Clear;
          SQL.Add('INSERT INTO TMP(NR_IMPORT,NR_SKLEP,ILOSC,CENA,WARTOSC,POZYCJA_PLIKU,KOD_KRESKOWY)');
          SQL.Add('VALUES(:A,:B,:C,:D,:E,:F,:G)');
          Unprepare;
             ParamByName('A').AsInteger := numer;
             ParamByName('B').AsInteger := sklep;
             ParamByName('C').AsFloat := ilosc;
             ParamByName('D').AsFloat := cena;
             ParamByName('E').AsFloat := wartosc;
             ParamByName('F').AsInteger := pozycja_pliku;
             ParamByName('G').AsString := kod_kreskowy;
          Prepare;
          Open;
     end;
if dm.Transakcje.InTransaction then dm.Transakcje.Commit;
end;

procedure Tdm.ZwiekszNr(numer: Integer);
begin
if not dm.Transakcje.InTransaction then dm.Transakcje.StartTransaction;
with dm.ibqNumery do
     begin
          Close;
          SQL.Clear;
          SQL.Add('INSERT INTO NUMERY(NUMER)');
          SQL.Add('VALUES(:A)');
          Unprepare;
             ParamByName('A').AsInteger := numer;
          Prepare;
          Open;
     end;
if dm.Transakcje.InTransaction then dm.Transakcje.Commit;
end;

procedure Tdm.PokazPozycje(nr_import: Integer);
begin
with dm.ibqPokazPozycje do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT A.NAZWA,A.ILOSC,A.CENA,A.WARTOSC,A.NAZWA_DOK_PIERW,A.AUTOMAT_RECZNY,A.DATA_ARKUSZA FROM ARKUSZ A,TOWARY B');
          SQL.Add('WHERE A.NR_TOWAR = B.NR_TOWARY AND A.NR_IMPORT = :A');
          SQL.Add('ORDER BY A.NAZWA');
          Unprepare;
             ParamByName('A').AsInteger := nr_import;
          Prepare;
          Open;
     end;
end;

procedure Tdm.KasujTowary;
begin
if not dm.Transakcje.InTransaction then dm.Transakcje.StartTransaction;
with dm.ibqTowary do
     begin
          Close;
          SQL.Clear;
          SQL.Add('DELETE FROM TOWARY');
          Open;
     end;
if dm.Transakcje.InTransaction then dm.Transakcje.Commit;
end;

function Tdm.SumujPozycje(nr_importu: Integer): Real;
begin
with dm.ibqSumujPozycje do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT SUM(WARTOSC) AS WARTOSC FROM ARKUSZ');
          SQL.Add('WHERE NR_IMPORT = :A');
          Unprepare;
             ParamByName('A').AsInteger := nr_importu;
          Prepare;
          Open;
          Result := FieldValues['WARTOSC'];
     end;
end;

procedure Tdm.PokazSklepy(nazwa: String);
begin
with dm.ibqSklepy do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT * FROM SKLEPY');
          SQL.Add('WHERE NAZWA = :A');
          Unprepare;
             ParamByName('A').AsString := nazwa;
          Prepare;
          Open;
     end;
end;

procedure Tdm.PokazPozycje(nr_import, sortowanie: Integer);
begin
with dm.ibqPokazPozycje do
     begin
          Close;
          SQL.Clear;
          //SQL.Add('SELECT A.NAZWA,A.ILOSC,A.CENA,A.WARTOSC,A.NAZWA_DOK_PIERW,A.AUTOMAT_RECZNY,A.DATA_ARKUSZA FROM ARKUSZ A,TOWARY B');
          SQL.Add('SELECT * FROM ARKUSZ A,TOWARY B');
          SQL.Add('WHERE A.NR_TOWAR = B.NR_TOWARY AND A.NR_IMPORT = :A');
          case sortowanie of
               1: SQL.Add('ORDER BY A.NAZWA');
               2: SQL.Add('ORDER BY A.DATA_ARKUSZA');
          end;
          Unprepare;
             ParamByName('A').AsInteger := nr_import;
          Prepare;
          Open;
     end;
end;

procedure Tdm.PokazArkusze(DataOd, DataDo: TDateTime);
begin
with dm.ibqPokazArkusze do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT A.NR_IMPORT AS IMPORT,A.NAZWA_DOK_PIERW AS DOKUMENT,B.NAZWA,A.DATA_ARKUSZA AS DATA,SUM(A.WARTOSC) AS WARTOSC FROM ARKUSZ A,SKLEPY B');
          SQL.Add('WHERE A.NR_SKLEP = B.NR_SKLEPY AND A.DATA_ARKUSZA BETWEEN :A AND :B');
          SQL.Add('GROUP BY A.NR_IMPORT,A.NAZWA_DOK_PIERW,B.NAZWA,A.DATA_ARKUSZA');
          SQL.Add('ORDER BY A.NR_IMPORT');
          Unprepare;
             ParamByName('A').AsDate := DataOd;
             ParamByName('B').AsDate := DataDo;
          Prepare;
          Open;
     end;
end;

procedure Tdm.PokazArkusze(NrOd, NrDo: Integer);
begin
with dm.ibqPokazArkusze do
     begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT A.NR_IMPORT AS IMPORT,A.NAZWA_DOK_PIERW AS DOKUMENT,B.NAZWA,A.DATA_ARKUSZA AS DATA,SUM(A.WARTOSC) AS WARTOSC FROM ARKUSZ A,SKLEPY B');
          SQL.Add('WHERE A.NR_SKLEP = B.NR_SKLEPY AND A.NR_IMPORT BETWEEN :A AND :B');
          SQL.Add('GROUP BY A.NR_IMPORT,A.NAZWA_DOK_PIERW,B.NAZWA,A.DATA_ARKUSZA');
          SQL.Add('ORDER BY A.NR_IMPORT');
          Unprepare;
             ParamByName('A').AsInteger := NrOd;
             ParamByName('B').AsInteger := NrDo;
          Prepare;
          Open;
     end;
end;

procedure Tdm.KasujPozycjeArkusz(nr_towaru, nr_import, nr_sklep: Integer);
begin
if not dm.Transakcje.InTransaction then dm.Transakcje.StartTransaction;
with dm.ibqZapiszArkusz do
     begin
          Close;
          SQL.Clear;
          SQL.Add('DELETE FROM ARKUSZ');
          SQL.Add('WHERE NR_TOWAR=:A AND NR_IMPORT=:B AND NR_SKLEP=:C');
          Unprepare;
             ParamByName('A').AsInteger := nr_towaru;
             ParamByName('B').AsInteger := nr_import;
             ParamByName('C').AsInteger := nr_sklep;
          Prepare;
          Open;
     end;
if dm.Transakcje.InTransaction then dm.Transakcje.Commit;
end;

procedure Tdm.PoprawArkusz(nr_towaru, nr_import, nr_sklep: Integer; ilosc,
  cena, wartosc: Real; nazwa, typ, dok_pierwotny: String);
begin
if not dm.Transakcje.InTransaction then dm.Transakcje.StartTransaction;
with dm.ibqZapiszArkusz do
     begin
          Close;
          SQL.Clear;
          SQL.Add('UPDATE ARKUSZ');
          SQL.Add('SET NAZWA=:G,ILOSC=:D,CENA=:E,WARTOSC=:F,NAZWA_DOK_PIERW=:H,AUTOMAT_RECZNY=:I');
          SQL.Add('WHERE NR_TOWAR=:A AND NR_IMPORT=:B AND NR_SKLEP=:C');
          Unprepare;
             ParamByName('A').AsInteger := nr_towaru;
             ParamByName('G').AsString := nazwa;
             ParamByName('B').AsInteger := nr_import;
             ParamByName('C').AsInteger := nr_sklep;
             ParamByName('D').AsFloat := ilosc;
             ParamByName('E').AsFloat := cena;
             ParamByName('F').AsFloat := wartosc;
             ParamByName('H').AsString := dok_pierwotny;
             ParamByName('I').AsString := typ;
          Prepare;
          Open;
     end;
if dm.Transakcje.InTransaction then dm.Transakcje.Commit;
end;

end.

