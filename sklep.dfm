object frSklep: TfrSklep
  Left = 624
  Top = 290
  Width = 351
  Height = 339
  Caption = 'Sklepy'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object JvToolBar1: TJvToolBar
    Left = 0
    Top = 0
    Width = 335
    Height = 42
    ButtonHeight = 38
    ButtonWidth = 39
    Caption = 'JvToolBar1'
    Flat = True
    Images = frGlowny.ImageList1
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Hint = 'Lista sklep'#243'w'
      Caption = 'ToolButton1'
      ImageIndex = 5
      ParentShowHint = False
      ShowHint = True
      OnClick = ToolButton1Click
    end
    object ToolButton2: TToolButton
      Left = 39
      Top = 0
      Hint = 'Dodaj sklep'
      Caption = 'ToolButton2'
      ImageIndex = 8
      ParentShowHint = False
      ShowHint = True
      OnClick = ToolButton2Click
    end
  end
  object pc: TPageControlEx
    Left = 0
    Top = 42
    Width = 335
    Height = 258
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 1
    HideHeader = True
    object TabSheet1: TTabSheet
      Caption = 'TabSheet1'
      object DBGrid1: TDBGrid
        Left = 0
        Top = 0
        Width = 335
        Height = 256
        Align = alClient
        DataSource = dm.dsSklepy
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnDblClick = DBGrid1DblClick
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'TabSheet2'
      ImageIndex = 1
      object LabeledEdit1: TLabeledEdit
        Left = 32
        Top = 32
        Width = 281
        Height = 21
        EditLabel.Width = 65
        EditLabel.Height = 13
        EditLabel.Caption = 'Nazwa sklepu'
        TabOrder = 0
      end
      object LabeledEdit2: TLabeledEdit
        Left = 32
        Top = 72
        Width = 121
        Height = 21
        EditLabel.Width = 40
        EditLabel.Height = 13
        EditLabel.Caption = 'Ulica i nr'
        TabOrder = 1
      end
      object LabeledEdit3: TLabeledEdit
        Left = 32
        Top = 120
        Width = 121
        Height = 21
        EditLabel.Width = 31
        EditLabel.Height = 13
        EditLabel.Caption = 'Miasto'
        TabOrder = 2
      end
      object LabeledEdit4: TLabeledEdit
        Left = 192
        Top = 120
        Width = 121
        Height = 21
        EditLabel.Width = 66
        EditLabel.Height = 13
        EditLabel.Caption = 'kod pocztowy'
        TabOrder = 3
      end
      object JvImgBtn1: TJvImgBtn
        Left = 110
        Top = 173
        Width = 113
        Height = 44
        Caption = 'Zapisz'
        TabOrder = 4
        OnClick = JvImgBtn1Click
        Flat = True
        Images = frGlowny.ImageList1
        ImageIndex = 7
      end
    end
  end
end
