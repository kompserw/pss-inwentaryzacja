object dm: Tdm
  OldCreateOrder = False
  Left = 499
  Top = 300
  Height = 497
  Width = 711
  object dsCSV: TDataSource
    DataSet = csv
    Left = 24
    Top = 24
  end
  object csv: TJvCsvDataSet
    CsvFieldDef = 'KOD_KRESKOWY:$15;CENA:&;ILOSC:&'
    HasHeaderRow = False
    Separator = ';'
    AutoBackupCount = 0
    StoreDefs = True
    Left = 72
    Top = 24
  end
  object serwer: TIBDatabase
    Connected = True
    DatabaseName = 'PSS.FDB'
    Params.Strings = (
      'user_name=sysdba'
      'password=masterkey')
    LoginPrompt = False
    DefaultTransaction = Transakcje
    IdleTimer = 0
    SQLDialect = 3
    TraceFlags = []
    Left = 24
    Top = 112
  end
  object Transakcje: TIBTransaction
    Active = True
    DefaultDatabase = serwer
    AutoStopAction = saNone
    Left = 24
    Top = 336
  end
  object ibqZapiszCSV: TIBQuery
    Database = serwer
    Transaction = Transakcje
    BufferChunks = 1000
    CachedUpdates = False
    Left = 128
    Top = 112
  end
  object csv_towary: TJvCsvDataSet
    CsvFieldDef = 'NAZWA:$50;KRESKOWY:$15;SMIEC:$15;CENA:&'
    HasHeaderRow = False
    Separator = ';'
    AutoBackupCount = 0
    StoreDefs = True
    Left = 184
    Top = 24
  end
  object dsCSV_towary: TDataSource
    DataSet = csv_towary
    Left = 264
    Top = 24
  end
  object ibqZapiszTowary: TIBQuery
    Database = serwer
    Transaction = Transakcje
    BufferChunks = 1000
    CachedUpdates = False
    Left = 128
    Top = 160
  end
  object ibqZapiszArkusz: TIBQuery
    Database = serwer
    Transaction = Transakcje
    BufferChunks = 1000
    CachedUpdates = False
    Left = 128
    Top = 216
  end
  object ibqNumery: TIBQuery
    Database = serwer
    Transaction = Transakcje
    BufferChunks = 1000
    CachedUpdates = False
    Left = 128
    Top = 280
  end
  object ibqTowary: TIBQuery
    Database = serwer
    Transaction = Transakcje
    BufferChunks = 1000
    CachedUpdates = False
    Left = 128
    Top = 336
  end
  object ibqPokazArkusz: TIBQuery
    Database = serwer
    Transaction = Transakcje
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      '')
    Left = 232
    Top = 216
  end
  object dsPokazArkusz: TDataSource
    DataSet = ibqPokazArkusz
    Left = 320
    Top = 216
  end
  object ibqTMP: TIBQuery
    Database = serwer
    Transaction = Transakcje
    BufferChunks = 1000
    CachedUpdates = False
    Left = 128
    Top = 392
  end
  object dsTMP_Pokaz: TDataSource
    DataSet = ibtTMP_Pokaz
    Left = 320
    Top = 392
  end
  object ibtTMP_Pokaz: TIBTable
    Database = serwer
    Transaction = Transakcje
    ForcedRefresh = True
    Active = True
    BufferChunks = 1000
    CachedUpdates = False
    FieldDefs = <
      item
        Name = 'NR_TMP'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'NR_IMPORT'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'NR_SKLEP'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'TOWAR'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'KOD_KRESKOWY'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'DATA_ARKUSZA'
        Attributes = [faRequired]
        DataType = ftDate
      end
      item
        Name = 'ILOSC'
        Attributes = [faRequired]
        DataType = ftBCD
        Precision = 18
        Size = 4
      end
      item
        Name = 'CENA'
        Attributes = [faRequired]
        DataType = ftBCD
        Precision = 18
        Size = 2
      end
      item
        Name = 'WARTOSC'
        DataType = ftBCD
        Precision = 18
        Size = 2
      end
      item
        Name = 'POZYCJA_PLIKU'
        Attributes = [faRequired]
        DataType = ftInteger
      end>
    IndexDefs = <
      item
        Name = 'PK_TMP'
        Fields = 'NR_TMP'
        Options = [ixUnique]
      end>
    StoreDefs = True
    TableName = 'TMP'
    Left = 256
    Top = 392
  end
  object ibqSklepy: TIBQuery
    Database = serwer
    Transaction = Transakcje
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      '')
    Left = 240
    Top = 112
  end
  object dsSklepy: TDataSource
    DataSet = ibqSklepy
    Left = 296
    Top = 112
  end
  object dsTowary: TDataSource
    DataSet = ibqTowary
    Left = 224
    Top = 336
  end
  object ibtSklepy: TIBTable
    Database = serwer
    Transaction = Transakcje
    Active = True
    BufferChunks = 1000
    CachedUpdates = False
    FieldDefs = <
      item
        Name = 'NR_SKLEPY'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'NAZWA'
        Attributes = [faRequired]
        DataType = ftString
        Size = 50
      end
      item
        Name = 'ULICA'
        Attributes = [faRequired]
        DataType = ftString
        Size = 50
      end
      item
        Name = 'MIASTO'
        Attributes = [faRequired]
        DataType = ftString
        Size = 50
      end
      item
        Name = 'KOD_P'
        Attributes = [faRequired]
        DataType = ftString
        Size = 6
      end>
    IndexDefs = <
      item
        Name = 'PK_SKLEPY'
        Fields = 'NR_SKLEPY'
        Options = [ixUnique]
      end>
    StoreDefs = True
    TableName = 'SKLEPY'
    Left = 400
    Top = 120
  end
  object dsSklepCombo: TDataSource
    DataSet = ibtSklepy
    Left = 464
    Top = 120
  end
  object ibqPokazPozycje: TIBQuery
    Database = serwer
    Transaction = Transakcje
    BufferChunks = 1000
    CachedUpdates = False
    Left = 464
    Top = 216
  end
  object dsPokazPozycje: TDataSource
    DataSet = ibqPokazPozycje
    Left = 544
    Top = 216
  end
  object ibqPokazArkusze: TIBQuery
    Database = serwer
    Transaction = Transakcje
    BufferChunks = 1000
    CachedUpdates = False
    Left = 232
    Top = 272
  end
  object dsPokazArkusze: TDataSource
    DataSet = ibqPokazArkusze
    Left = 320
    Top = 272
  end
  object ibqSumujPozycje: TIBQuery
    Database = serwer
    Transaction = Transakcje
    BufferChunks = 1000
    CachedUpdates = False
    Left = 624
    Top = 216
  end
end
