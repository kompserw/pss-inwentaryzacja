object frPokazTrescPliku: TfrPokazTrescPliku
  Left = 341
  Top = 220
  Width = 1142
  Height = 654
  Caption = 'Tre'#347#263' pliku importu towar'#243'w'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object JvEditor1: TJvEditor
    Left = 0
    Top = 57
    Width = 1126
    Height = 558
    Cursor = crIBeam
    Completion.ItemHeight = 13
    Completion.CRLF = '/n'
    Completion.Separator = '='
    TabStops = '3 5'
    BracketHighlighting.StringEscape = #39#39
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
  end
  object JvPanel1: TJvPanel
    Left = 0
    Top = 0
    Width = 1126
    Height = 57
    Align = alTop
    TabOrder = 1
    object JvLabel1: TJvLabel
      Left = 8
      Top = 11
      Width = 506
      Height = 38
      Caption = 
        'Zapami'#281'taj numery kolumn z nazw'#261', kodem kreskowym i cen'#261' towaru.' +
        #13#10'Numery zaczynaj'#261' si'#281' od 0. Ka'#380'd'#261' kolumn'#281' ko'#324'czy znak ;'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      HotTrackFont.Charset = DEFAULT_CHARSET
      HotTrackFont.Color = clWindowText
      HotTrackFont.Height = -16
      HotTrackFont.Name = 'Tahoma'
      HotTrackFont.Style = []
    end
    object LabeledEdit1: TLabeledEdit
      Left = 632
      Top = 24
      Width = 121
      Height = 21
      EditLabel.Width = 124
      EditLabel.Height = 13
      EditLabel.Caption = 'Nr kolumny nazwa towaru'
      TabOrder = 0
    end
    object LabeledEdit2: TLabeledEdit
      Left = 760
      Top = 24
      Width = 121
      Height = 21
      EditLabel.Width = 115
      EditLabel.Height = 13
      EditLabel.Caption = 'Nr kolmny kod kreskowy'
      TabOrder = 1
    end
    object LabeledEdit3: TLabeledEdit
      Left = 888
      Top = 24
      Width = 121
      Height = 21
      EditLabel.Width = 79
      EditLabel.Height = 13
      EditLabel.Caption = 'Nr kolumny cena'
      TabOrder = 2
    end
    object JvImgBtn1: TJvImgBtn
      Left = 1016
      Top = 8
      Width = 83
      Height = 41
      Caption = 'Dalej >>'
      TabOrder = 3
      OnClick = JvImgBtn1Click
      Flat = True
      Images = frGlowny.ImageList1
      ImageIndex = 9
    end
  end
end
