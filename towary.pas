unit towary;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, ComCtrls, PageControlEx, ImgList, ToolWin;

type
  TfrTowary = class(TForm)
    ToolBar1: TToolBar;
    ImageList1: TImageList;
    ToolButton1: TToolButton;
    StatusBar1: TStatusBar;
    pc: TPageControlEx;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    DBGrid1: TDBGrid;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    od: TOpenDialog;
    ProgressBar1: TProgressBar;
    DBGrid2: TDBGrid;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure ToolButton3Click(Sender: TObject);
    procedure ToolButton4Click(Sender: TObject);
    procedure ToolButton6Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure StatusBar1DrawPanel(StatusBar: TStatusBar;
      Panel: TStatusPanel; const Rect: TRect);
    procedure UstawSiatkeTowary;
    procedure UstawPole1(nr: Integer);
    procedure UstawPole2(nr: Integer);
    procedure UstawPole3(nr: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frTowary: TfrTowary;
  Pole1,Pole2,Pole3: Integer;

implementation
uses baza, sklep, tresc_pliku;
{$R *.dfm}

procedure TfrTowary.ToolButton1Click(Sender: TObject);
begin
dm.PokazTowary(0);
UstawSiatkeTowary;
//DBGrid1.DataSource.Name := 'dsTowary';
pc.ActivePageIndex := 0;
end;

procedure TfrTowary.ToolButton2Click(Sender: TObject);
begin
pc.ActivePageIndex := 1;
end;

procedure TfrTowary.ToolButton3Click(Sender: TObject);
begin
pc.ActivePageIndex := 2;
end;

procedure TfrTowary.ToolButton4Click(Sender: TObject);
begin
if MessageBox(Handle,'Skasowa� w bazie towary ?', 'Czyszczenie bazy towar�w', MB_YESNO + MB_ICONQUESTION) = IdYes then
   dm.KasujTowary;
end;

procedure TfrTowary.ToolButton6Click(Sender: TObject);
var
   sklep,i,max: Integer;
   kod_kreskowy,nazwa: String;
   cena: Real;
begin
if od.Execute then
begin
pc.ActivePageIndex := 1;
frSklep.ShowModal;
sklep := dm.ibqSklepy.FieldValues['NR_SKLEPY'];
if MessageBox(Handle,'Skasowa� w bazie towary z tego sklepu ?', 'Czyszczenie bazy towar�w', MB_YESNO + MB_ICONQUESTION) = IdYes then
   dm.KasujTowary(sklep);
dm.csv_towary.FileName := od.FileName;
if Length(od.FileName) > 0 then
   begin

        frPokazTrescPliku.UstawPlik(od.FileName);
        frPokazTrescPliku.WyswietlPlik;
        frPokazTrescPliku.ShowModal;
        //frKtorePola.ShowModal;
        dm.csv_towary.Active := True;
        dm.csv_towary.First;        
        ProgressBar1.Min := 0;
        max := dm.csv_towary.RecordCount;
        ProgressBar1.Max := max;
        i := 0;
        while not dm.csv_towary.Eof do
              begin
                   nazwa := dm.csv_towary.Fields.Fields[Pole1].AsString;
                   if Length(nazwa) > 50 then Delete(nazwa,50,50);
                   kod_kreskowy := dm.csv_towary.Fields.Fields[Pole2].AsString;
                   if Length(kod_kreskowy) > 15 then Delete(kod_kreskowy,15,Length(kod_kreskowy) - 15);
                   if Pole3 >= 0 then
                   if (dm.csv_towary.Fields.Fields[Pole3].IsNull) then
                      cena := 0
                   else
                       cena := StrToFloat(StringReplace(dm.csv_towary.Fields.Fields[Pole3].AsString,'.',',',[rfReplaceAll]));
                   dm.ZapiszCSV_Towary(sklep,nazwa,kod_kreskowy,cena);
                   i := i + 1;
                   ProgressBar1.Position := i;
                   dm.csv_towary.Next;
              end;
   end;
end;
end;

procedure TfrTowary.FormCreate(Sender: TObject);
var
   ProgressBarStyle: integer;
begin
StatusBar1.Panels[2].Style := psOwnerDraw;
ProgressBar1.Parent := StatusBar1;
ProgressBarStyle := GetWindowLong(ProgressBar1.Handle,GWL_EXSTYLE);
ProgressBarStyle := ProgressBarStyle - WS_EX_STATICEDGE;
SetWindowLong(ProgressBar1.Handle,GWL_EXSTYLE,ProgressBarStyle);
pc.ActivePageIndex := 0;
dm.PokazTowary(0);
UstawSiatkeTowary;
end;

procedure TfrTowary.StatusBar1DrawPanel(StatusBar: TStatusBar;
  Panel: TStatusPanel; const Rect: TRect);
begin
if Panel = StatusBar.Panels[2] then
with ProgressBar1 do begin
     Top := Rect.Top;
     Left := Rect.Left;
     Width := Rect.Right - Rect.Left - 15;
     Height := Rect.Bottom - Rect.Top;
end;
end;

procedure TfrTowary.UstawSiatkeTowary;
begin
with DBGrid1.Columns do
     begin
          Items[0].Visible := False;
          Items[1].Visible := False;
          Items[6].Visible := False;
          Items[8].Visible := False;
          Items[9].Visible := False;
          Items[10].Visible := False;
          Items[2].Title.Caption := 'Nazwa towaru';
          Items[3].Title.Caption := 'Kod kreskowy';
          Items[4].Title.Caption := 'Cena sprzeda�y';
          Items[5].Title.Caption := 'Cena zakupu';
          Items[7].Title.Caption := 'Nazwa sklepu';
          Items[2].Width := 250;
          Items[3].Width := 150;
          Items[4].Width := 100;
          Items[5].Width := 100;
          Items[7].Width := 100;
     end;
end;

procedure TfrTowary.UstawPole1(nr: Integer);
begin
     Pole1 := nr;
end;

procedure TfrTowary.UstawPole2(nr: Integer);
begin
     Pole2 := nr;
end;

procedure TfrTowary.UstawPole3(nr: Integer);
begin
     Pole3 := nr;
end;

end.
