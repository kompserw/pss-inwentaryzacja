unit sklep;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, ComCtrls, PageControlEx, ToolWin, JvExComCtrls,
  JvToolBar, StdCtrls, JvExStdCtrls, JvButton, JvCtrls, ExtCtrls;

type
  TfrSklep = class(TForm)
    JvToolBar1: TJvToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    pc: TPageControlEx;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGrid1: TDBGrid;
    LabeledEdit1: TLabeledEdit;
    LabeledEdit2: TLabeledEdit;
    LabeledEdit3: TLabeledEdit;
    LabeledEdit4: TLabeledEdit;
    JvImgBtn1: TJvImgBtn;
    procedure FormCreate(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure JvImgBtn1Click(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure UstawSiatke;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frSklep: TfrSklep;

implementation
uses glowny,baza;
{$R *.dfm}

procedure TfrSklep.FormCreate(Sender: TObject);
begin
dm.PokazSklepy;
UstawSiatke;
end;

procedure TfrSklep.ToolButton1Click(Sender: TObject);
begin
dm.PokazSklepy;
UstawSiatke;
pc.ActivePageIndex := 0;
end;

procedure TfrSklep.ToolButton2Click(Sender: TObject);
begin
pc.ActivePageIndex := 1;
end;

procedure TfrSklep.JvImgBtn1Click(Sender: TObject);
begin
dm.DodajSklep(LabeledEdit1.Text,LabeledEdit2.Text,LabeledEdit3.Text,LabeledEdit4.Text);
LabeledEdit1.Text := '';LabeledEdit2.Text := '';LabeledEdit3.Text := '';LabeledEdit4.Text := '';
dm.PokazSklepy;
UstawSiatke;
pc.ActivePageIndex := 0;
end;

procedure TfrSklep.DBGrid1DblClick(Sender: TObject);
begin
dm.PokazSklepy(dm.ibqSklepy.FieldValues['NR_SKLEPY']);
Close;
end;

procedure TfrSklep.UstawSiatke;
begin
with DBGrid1.Columns do
     begin
          Items[1].FieldName := 'NAZWA';
          Items[1].Title.Caption := 'Nazwa sklepu';
          Items[1].Width := 300;
          Items[0].Visible := False;
          Items[2].Visible := False;
          Items[3].Visible := False;
          Items[4].Visible := False;
     end;
end;

end.
