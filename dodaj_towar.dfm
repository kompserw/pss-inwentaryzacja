object frDodajTowar: TfrDodajTowar
  Left = 486
  Top = 306
  Width = 312
  Height = 164
  Caption = 'Dodaj nowy towar'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object LabeledEdit1: TLabeledEdit
    Left = 24
    Top = 32
    Width = 121
    Height = 21
    EditLabel.Width = 69
    EditLabel.Height = 13
    EditLabel.Caption = 'Nazwa towaru'
    TabOrder = 0
  end
  object LabeledEdit2: TLabeledEdit
    Left = 160
    Top = 32
    Width = 121
    Height = 21
    EditLabel.Width = 66
    EditLabel.Height = 13
    EditLabel.Caption = 'Kod kreskowy'
    TabOrder = 1
  end
  object JvImgBtn1: TJvImgBtn
    Left = 96
    Top = 72
    Width = 105
    Height = 41
    Caption = 'Zapisz'
    TabOrder = 2
    Flat = True
    Images = frGlowny.ImageList1
    ImageIndex = 7
  end
end
