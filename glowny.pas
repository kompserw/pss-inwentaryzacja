unit glowny;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ComCtrls, ImgList, ToolWin, DB, JvComponentBase,
  JvCSVBaseControls, Grids, DBGrids, JvCsvData, PageControlEx, ExtCtrls,
  DBCtrls, StdCtrls, JvExStdCtrls, JvButton, JvCtrls, frxClass, frxDBSet,
  frxIBXComponents, frxExportPDF, frxExportXLSX, ShellApi, INIFiles,
  JvEdit, JvExComCtrls, JvDateTimePicker, JvExControls, JvLabel, JvRichEdit,
  JvMenus, XDBGrids, XDBLists;

type
  TfrGlowny = class(TForm)
    MainMenu1: TMainMenu;
    Pliki1: TMenuItem;
    Wczytajzkolektora1: TMenuItem;
    Pokawczytany1: TMenuItem;
    Drukujwczytany1: TMenuItem;
    Pokacayremament1: TMenuItem;
    Drukujcayremament1: TMenuItem;
    N1: TMenuItem;
    Koniec1: TMenuItem;
    Administracja1: TMenuItem;
    Sowniki1: TMenuItem;
    Sklepy1: TMenuItem;
    owary1: TMenuItem;
    sb: TStatusBar;
    ToolBar1: TToolBar;
    ImageList1: TImageList;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    od: TOpenDialog;
    pc: TPageControlEx;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    Panel3: TPanel;
    Panel4: TPanel;
    JvImgBtn1: TJvImgBtn;
    Siatka: TStringGrid;
    frxReport1: TfrxReport;
    frxIBXComponents1: TfrxIBXComponents;
    frxDBDataset1: TfrxDBDataset;
    ToolButton9: TToolButton;
    TabSheet3: TTabSheet;
    Panel5: TPanel;
    LabeledEdit1: TLabeledEdit;
    LabeledEdit2: TLabeledEdit;
    GroupBox1: TGroupBox;
    DBGrid2: TDBGrid;
    GroupBox2: TGroupBox;
    LabeledEdit3: TLabeledEdit;
    LabeledEdit4: TLabeledEdit;
    LabeledEdit5: TLabeledEdit;
    LabeledEdit6: TLabeledEdit;
    JvImgBtn2: TJvImgBtn;
    GroupBox3: TGroupBox;
    LabeledEdit7: TLabeledEdit;
    LabeledEdit8: TLabeledEdit;
    JvImgBtn3: TJvImgBtn;
    GroupBox4: TGroupBox;
    ComboBox1: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    frxXLSXExport1: TfrxXLSXExport;
    frxPDFExport1: TfrxPDFExport;
    GroupBox5: TGroupBox;
    DBGrid3: TDBGrid;
    JvImgBtn4: TJvImgBtn;
    frxDBDataset2: TfrxDBDataset;
    Kasowaniebaz1: TMenuItem;
    ProgressBar1: TProgressBar;
    ToolButton10: TToolButton;
    TabSheet4: TTabSheet;
    Panel2: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    DBGrid4: TDBGrid;
    Dodatkoweopcje1: TMenuItem;
    Pokaremament1: TMenuItem;
    DrukujRemament1: TMenuItem;
    N2: TMenuItem;
    Label3: TLabel;
    Label4: TLabel;
    dtpOd: TJvDateTimePicker;
    dtpDo: TJvDateTimePicker;
    jeNrOd: TJvEdit;
    jeNrDo: TJvEdit;
    JvLabel1: TJvLabel;
    JvLabel2: TJvLabel;
    JvLabel3: TJvLabel;
    JvLabel4: TJvLabel;
    JvRichEdit1: TJvRichEdit;
    JvPopupMenu1: TJvPopupMenu;
    Poprawpozycj1: TMenuItem;
    Usupozycj1: TMenuItem;
    Dodajpozycj1: TMenuItem;
    N3: TMenuItem;
    Drukujtendokument1: TMenuItem;
    XDBGrid1: TXDBGrid;
    XDBColumnsDialog1: TXDBColumnsDialog;
    procedure FormCreate(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton8Click(Sender: TObject);
    procedure JvImgBtn1Click(Sender: TObject);
    procedure DBNavigator1DblClick(Sender: TObject);
    procedure UstawSiatke(ile_wierszy: Integer);
    procedure UstawSiatkeReczny;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure sbDrawPanel(StatusBar: TStatusBar; Panel: TStatusPanel;
      const Rect: TRect);
    procedure ToolButton3Click(Sender: TObject);
    procedure LabeledEdit1Change(Sender: TObject);
    procedure LabeledEdit2Change(Sender: TObject);
    procedure JvImgBtn3Click(Sender: TObject);
    procedure ComboBox1DropDown(Sender: TObject);
    procedure DBGrid2CellClick(Column: TColumn);
    procedure JvImgBtn2Click(Sender: TObject);
    procedure ToolButton9Click(Sender: TObject);
    procedure pcChanging(Sender: TObject; var AllowChange: Boolean);
    function SprawdzSklep(): Boolean;
    procedure JvImgBtn4Click(Sender: TObject);
    procedure Kasowaniebaz1Click(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure Pokawczytany1Click(Sender: TObject);
    procedure DBGrid4CellClick(Column: TColumn);
    procedure owary1Click(Sender: TObject);
    procedure Dodatkoweopcje1Click(Sender: TObject);
    procedure Pokaremament1Click(Sender: TObject);
    procedure DrukujRemament1Click(Sender: TObject);
    procedure TabSheet4Hide(Sender: TObject);
    procedure TabSheet4Show(Sender: TObject);
    procedure UstawSiatkeArkuszePozycje(aGrid: TDBGrid);
    procedure UstawSiatkeArkuszePozycje1(aGrid: TXDBGrid);
    procedure UstawSiatkeArkuszeLista(aGrid: TDBGrid);
    procedure UstawSiatkeImport(aGrid: TDBGrid);
    procedure DBGrid5DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid4DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure dtpDoChange(Sender: TObject);
    procedure jeNrDoChange(Sender: TObject);
    procedure DBGrid5CellClick(Column: TColumn);
    procedure Dodajpozycj1Click(Sender: TObject);
    procedure Poprawpozycj1Click(Sender: TObject);
    procedure Usupozycj1Click(Sender: TObject);
    procedure Drukujtendokument1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frGlowny: TfrGlowny;

implementation

uses baza, sklep, towary, edycja_pozycji;

{$R *.dfm}

procedure TfrGlowny.FormCreate(Sender: TObject);
var
   ProgressBarStyle: integer;
begin
Pokaremament1.Visible := False;
DrukujRemament1.Visible := False;
ToolButton6.Visible := False;
ToolButton4.Visible := False;
dtpOd.Date := now;
dtpDo.Date := now;
//jeNrOd.Text := '';
//jeNrDo.Text := '';
sb.Panels[2].Style := psOwnerDraw;
ProgressBar1.Parent := sb;
ProgressBarStyle := GetWindowLong(ProgressBar1.Handle,GWL_EXSTYLE);
ProgressBarStyle := ProgressBarStyle - WS_EX_STATICEDGE;
SetWindowLong(ProgressBar1.Handle,GWL_EXSTYLE,ProgressBarStyle);
pc.ActivePageIndex := 0;
end;

procedure TfrGlowny.UstawSiatke(ile_wierszy: Integer);
var
   x: Integer;
begin
Siatka.RowCount := ile_wierszy;
Siatka.ColCount := 9;
for x:=1 to ile_wierszy do
    Siatka.Cells[0,x] := IntToStr(x);
Siatka.Cells[1,0] := 'Nr importu';
Siatka.Cells[2,0] := 'Nr sklepu';
Siatka.Cells[3,0] := 'Nazwa towaru';
Siatka.ColWidths[3] := 250;
Siatka.Cells[4,0] := 'Kod kreskowy';
Siatka.ColWidths[4] := 150;
Siatka.Cells[5,0] := 'Ilo��';
Siatka.Cells[6,0] := 'Cena';
Siatka.Cells[7,0] := 'Warto��';
Siatka.Cells[8,0] := 'Poz.w pliku';

end;

procedure TfrGlowny.ToolButton1Click(Sender: TObject);
var
   sklep,numer,nr_towar,bledne,x,y: Integer;
   kod_kreskowy,nazwa,automat,dok_pierwotny: String;
   cena,ilosc,wartosc,suma: Real;
begin
sb.Panels.Items[0].Text := '';
sb.Panels.Items[1].Text := '';
pc.ActivePageIndex := 0;
sklep := frSklep.ShowModal;
if not dm.ibqSklepy.FieldByName('NR_SKLEPY').IsNull then sklep := dm.ibqSklepy.FieldValues['NR_SKLEPY']
   else
       begin
            ShowMessage('Brak sklepu, przerywam import');
            Exit;
       end;
if od.Execute then
begin
dm.csv.FileName := od.FileName;
dm.csv.Active := True;
dm.csv.First;
numer := dm.PodajNr;
numer := numer + 1;
bledne := 1;
ProgressBar1.Min := 0;
ProgressBar1.Max := dm.csv.RecordCount;
ProgressBar1.StepBy(10);
y := 0;
dok_pierwotny := InputBox('Podaj nr dokumentu pierwotnego', 'Podaj numer', '');
while not dm.csv.Eof do
      begin
           y := y + 1;
           ProgressBar1.Position := y;
           kod_kreskowy := dm.csv.Fields.Fields[0].AsString;
           cena := StrToFloat(StringReplace(dm.csv.Fields.Fields[2].AsString,'.',',',[rfReplaceAll]));
           ilosc := StrToFloat(StringReplace(dm.csv.Fields.Fields[1].AsString,'.',',',[rfReplaceAll]));
           dm.ZapiszCSV(numer,sklep,kod_kreskowy,cena,ilosc);
           nr_towar := dm.WyszukajTowary(kod_kreskowy,sklep);
           nazwa := dm.NazwaTowaru(nr_towar);
           wartosc := cena * ilosc;
           automat := 'A';
           Label4.Caption := dok_pierwotny;
           if nr_towar > 0 then
              dm.ZapiszArkusz(nr_towar,numer,sklep,ilosc,cena,wartosc,nazwa,automat,dok_pierwotny)
           else
               begin
                    dm.ZapiszTMP(numer,sklep,y,ilosc,cena,wartosc,kod_kreskowy);
                    sb.SimpleText := 'Znaleziono niezidentyfikowanych towar�w: ' + IntToStr(bledne);
                    bledne := bledne + 1;
                    pc.ActivePageIndex := 1;
                    UstawSiatke(bledne);
                    dm.PokazTMP;
                    dm.ibqTMP.First;
                    for x:=1 to bledne do
                        with dm.ibqTMP,Siatka do
                             begin
                                  if not FieldByName('NR_IMPORT').IsNull then Cells[1,x] := IntToStr(FieldValues['NR_IMPORT'])
                                     else Cells[1,x] := 'brak';
                                  if not FieldByName('NR_SKLEP').IsNull then Cells[2,x] := IntToStr(FieldValues['NR_SKLEP'])
                                     else Cells[2,x] := 'brak';
                                  if not FieldByName('TOWAR').IsNull then Cells[3,x] := FieldValues['TOWAR']
                                     else Cells[3,x] := 'brak';
                                  if not FieldByName('KOD_KRESKOWY').IsNull then Cells[4,x] := FieldValues['KOD_KRESKOWY']
                                     else Cells[4,x] := 'brak';
                                  if not FieldByName('ILOSC').IsNull then Cells[5,x] := FloatToStr(FieldValues['ILOSC'])
                                     else Cells[5,x] := 'brak';
                                  if not FieldByName('CENA').IsNull then Cells[6,x] := FloatToStr(FieldValues['CENA'])
                                     else Cells[6,x] := 'brak';
                                  if not FieldByName('WARTOSC').IsNull then Cells[7,x] := FloatToStr(FieldValues['WARTOSC'])
                                     else Cells[7,x] := 'brak';
                                  if not FieldByName('POZYCJA_PLIKU').IsNull then Cells[8,x] := FieldValues['POZYCJA_PLIKU']
                                     else Cells[8,x] := 'brak';
                                  Next;
                             end;
               end;
           dm.csv.Next;
      end;
dm.ZwiekszNr(numer);      
dm.PokazArkusz(numer);
UstawSiatkeImport(DBGrid1);
dm.csv.Close;
end;

suma := dm.SumujPozycje(numer);
sb.Panels.Items[0].Text := 'Warto�� pozycji na li�cie';
sb.Panels.Items[1].Alignment := taRightJustify;
sb.Panels.Items[1].Text := FloatToStrF(suma, ffCurrency, 7, 2);

ProgressBar1.Position := 0;
pc.ActivePageIndex := 0;
end;

procedure TfrGlowny.ToolButton8Click(Sender: TObject);
begin
frTowary.pc.ActivePageIndex := 0;
frTowary.ShowModal;
end;

procedure TfrGlowny.JvImgBtn1Click(Sender: TObject);
var
   x,y: Integer;
   typ,dok_pierwotny: String;
begin
x := Siatka.RowCount - 1;
typ := 'R';
dok_pierwotny := Label4.Caption;
dm.KopiujTMP(x,typ,dok_pierwotny);
for y := 0 to Siatka.ColCount do
    Siatka.Cols[y].Clear;
pc.ActivePageIndex := 0;
ProgressBar1.Position := 0;
sb.SimpleText := '';
end;

procedure TfrGlowny.DBNavigator1DblClick(Sender: TObject);
begin
dm.ibtTMP_Pokaz.Active := True;
end;

procedure TfrGlowny.FormClose(Sender: TObject; var Action: TCloseAction);
begin
dm.KasujTMP;
end;

procedure TfrGlowny.sbDrawPanel(StatusBar: TStatusBar; Panel: TStatusPanel;
  const Rect: TRect);
begin
if Panel = sb.Panels[2] then
with ProgressBar1 do begin
     Top := Rect.Top;
     Left := Rect.Left;
     Width := Rect.Right - Rect.Left - 15;
     Height := Rect.Bottom - Rect.Top;
end;
end;

procedure TfrGlowny.ToolButton3Click(Sender: TObject);
var
   nazwa: String;
begin
//frxReport1.LoadFromFile('inw_czastkowe.fr3');

nazwa := dm.ibqPokazArkusze.FieldValues['NAZWA'];
dm.PokazSklepy(nazwa);
frxReport1.ShowReport();
ToolButton3.Enabled := False;
end;

procedure TfrGlowny.LabeledEdit1Change(Sender: TObject);
var
   nazwa: String;
   sklep: Integer;
begin
sklep := 11;
nazwa := LabeledEdit1.Text;
dm.SzukajTowar(1,sklep,nazwa);

end;

procedure TfrGlowny.LabeledEdit2Change(Sender: TObject);
var
   kod: String;
   sklep: Integer;
begin
sklep := 11;
kod := LabeledEdit2.Text;
dm.SzukajTowar(2,sklep,kod);

end;

procedure TfrGlowny.JvImgBtn3Click(Sender: TObject);
var
   nr_towaru,nr_importu,nr_sklep: Integer;
   ilosc,cena,wartosc: Real;
   nazwa,automat,dok_pierwotny: String;
begin
if SprawdzSklep() then
begin
nr_towaru := dm.ibqTowary.FieldValues['NR_TOWARY'];
nazwa := dm.ibqTowary.FieldValues['NAZWA'];
nr_importu := dm.PodajNr;
nr_sklep := dm.NrSklepu(ComboBox1.Text);
ilosc := StrToFloat(LabeledEdit7.Text);
cena :=  StrToFloat(LabeledEdit8.Text);
wartosc := ilosc * cena;
automat := 'R';
dok_pierwotny := Label4.Caption;
dm.ZapiszArkusz(nr_towaru,nr_importu,nr_sklep,ilosc,cena,wartosc,nazwa,automat,dok_pierwotny);
dm.SzukajTowar(1,0,'');
UstawSiatkeReczny;
ComboBox1.Clear;
ComboBox1.Text := 'Lista sklep�w';
LabeledEdit7.Text := '';
LabeledEdit8.Text := '';
LabeledEdit1.Text := '';
LabeledEdit2.Text := '';
label1.Caption := '';
label2.Caption := '';
end
else ShowMessage('Wybierz sklep z listy');
end;

procedure TfrGlowny.ComboBox1DropDown(Sender: TObject);
begin
dm.PokazSklepy;
dm.ibqSklepy.First;
while not dm.ibqSklepy.Eof do
      begin
           ComboBox1.Items.Add(dm.ibqSklepy.FieldValues['NAZWA']);
           dm.ibqSklepy.Next;
      end;
end;

procedure TfrGlowny.DBGrid2CellClick(Column: TColumn);
begin
label1.Caption := dm.ibqTowary.FieldValues['NAZWA'];
label2.Caption := dm.ibqTowary.FieldValues['KOD_KRESKOWY'];
end;

procedure TfrGlowny.JvImgBtn2Click(Sender: TObject);
var
   nr_towaru,nr_importu,nr_sklep: Integer;
   ilosc,cena,wartosc: Real;
   nazwa,kod_kreskowy,automat,dok_pierwotny: String;
begin
if SprawdzSklep() then
begin
nr_towaru := dm.NrTowaru('');
nazwa := LabeledEdit3.Text;
kod_kreskowy := LabeledEdit4.Text;
nr_importu := dm.PodajNr;
nr_sklep := dm.NrSklepu(ComboBox1.Text);
ilosc := StrToFloat(LabeledEdit6.Text);
cena :=  StrToFloat(LabeledEdit5.Text);
wartosc := ilosc * cena;
automat := 'R';
dok_pierwotny := Label4.Caption;
dm.ZapiszCSV_Towary(nr_sklep,nazwa,kod_kreskowy,cena);
dm.ZapiszArkusz(nr_towaru,nr_importu,nr_sklep,ilosc,cena,wartosc,nazwa,automat,dok_pierwotny);
dm.PokazArkusz(nr_importu);
dm.SzukajTowar(1,0,'');
UstawSiatkeReczny;
LabeledEdit3.Text := '';
LabeledEdit4.Text := '';
LabeledEdit5.Text := '';
LabeledEdit6.Text := '';
end
else ShowMessage('Wybierz sklep z listy');
end;

procedure TfrGlowny.UstawSiatkeReczny;
begin
with DBGrid2.Columns do
     begin
          Items[2].Visible := False;
          Items[0].Title.Caption := 'Nazwa towaru';
          Items[1].Title.Caption := 'Kod kreskowy';
          Items[0].Width := 250;
          Items[1].Width := 100;
     end;
end;

procedure TfrGlowny.ToolButton9Click(Sender: TObject);
begin
dm.SzukajTowar(1,0,'');
dm.ZwiekszNr(dm.PodajNr + 1);
pc.ActivePageIndex := 2;
end;

procedure TfrGlowny.pcChanging(Sender: TObject; var AllowChange: Boolean);
begin
dm.SzukajTowar(1,0,'');
end;

function TfrGlowny.SprawdzSklep: Boolean;
begin
if dm.SprawdzSklep(ComboBox1.Text) then Result := True
   else Result := False;
end;

procedure TfrGlowny.JvImgBtn4Click(Sender: TObject);
var
   nr_towaru,nr_importu,nr_sklep: Integer;
   ilosc,cena,wartosc: Real;
   nazwa,kod_kreskowy,automat,dok_pierwotny: String;
   TF: TextFile;
begin

if SprawdzSklep() then
begin
nr_towaru := dm.NrTowaru('');
nazwa := LabeledEdit3.Text;
kod_kreskowy := LabeledEdit4.Text;
nr_importu := dm.PodajNr;
AssignFile(TF,ExtractFilePath(Application.ExeName) + 'R'+IntToStr(nr_importu)+'.txt');
nr_sklep := dm.NrSklepu(ComboBox1.Text);
ilosc := StrToFloat(LabeledEdit6.Text);
cena :=  StrToFloat(LabeledEdit5.Text);
wartosc := ilosc * cena;
automat := 'R';
dok_pierwotny := Label4.Caption;
dm.ZapiszCSV_Towary(nr_sklep,nazwa,kod_kreskowy,cena);
dm.ZapiszArkusz(nr_towaru,nr_importu,nr_sklep,ilosc,cena,wartosc,nazwa,automat,dok_pierwotny);
dm.ZwiekszNr(nr_importu + 1);
dm.PokazArkusz(nr_importu);
dm.PokazSklepy(nr_sklep);
dm.SzukajTowar(1,0,'');
UstawSiatkeReczny;
ComboBox1.Clear;
ComboBox1.Text := 'Lista sklep�w';
LabeledEdit3.Text := '';
LabeledEdit4.Text := '';
LabeledEdit5.Text := '';
LabeledEdit6.Text := '';
end
else ShowMessage('Wybierz sklep z listy');
end;

procedure TfrGlowny.Kasowaniebaz1Click(Sender: TObject);
var
   haslo: String;
begin
haslo := InputBox('Podaj has�o do rozpocz�cia operacji', 'Podaj has�o', '');
if CompareStr(haslo, 'KompSerw086') = 0 then
   begin
        dm.KasujBazy;
        ShowMessage('Bazy zosta�y wyczyszczone');
   end
else
    ShowMessage('B��dne has�o, podaj jeszcze raz');
end;

procedure TfrGlowny.ToolButton2Click(Sender: TObject);
var
   INI: TINIFile;
   sklep,numer,nr_towar,bledne,x,y,opcja: Integer;
   kod_kreskowy,nazwa,plik1,com,szybkosc,program_zew,calosc,automat,dok_pierwotny: String;
   cena,ilosc,wartosc: Real;
begin
if not FileExists(ExtractFilePath(Application.ExeName)+'konfig.ini') then
   begin
        INI := TINIFile.Create(ExtractFilePath(Application.ExeName)+'konfig.ini');
        try
           INI.WriteString('Port COM','com','COM1');
           INI.WriteString('Predkosc COM','predkosc','115200');
           INI.WriteString('Program','uruchom','ir_read.exe');
           INI.WriteString('Calosc','ciag','A.TXT,COM3,11520,1,1,1,0,0,0');
           INI.WriteInteger('Opcje','Nr opcji',1);
        finally
           INI.Free;
        end;
   end
else
   begin
        INI := TINIFile.Create(ExtractFilePath(Application.ExeName)+'konfig.ini');
        try
           com := INI.ReadString('Port COM','com','COM1');
           szybkosc := INI.ReadString('Predkosc COM','predkosc','115200');
           program_zew := INI.ReadString('Program','uruchom','ir_read.exe');
           calosc := INI.ReadString('Calosc','ciag','A.TXT,COM3,11520,1,1,1,0,0,0');
           opcja := INI.ReadInteger('Opcje','Nr opcji',1);
        finally
           INI.Free;
        end;
   end;

pc.ActivePageIndex := 0;
sklep := frSklep.ShowModal;
if not dm.ibqSklepy.FieldByName('NR_SKLEPY').IsNull then sklep := dm.ibqSklepy.FieldValues['NR_SKLEPY']
   else
       begin
            ShowMessage('Brak sklepu, przerywam import');
            Exit;
       end;

numer := dm.PodajNr;
//*****************************************  obs�uga kolektora ***********************************
case opcja of
     1: begin
             plik1 := 'A' + IntToStr(numer) + '.TXT,' + com + ',' + szybkosc + ',1,1,1,0,0';
             ShellExecute (Handle, 'open', '232_read.exe', PChar(plik1), nil, SW_SHOWNORMAL);
        end;
     2: ShellExecute (Handle, 'open',PChar(program_zew), PChar(calosc), nil, SW_SHOWNORMAL);
end;

//***************************************** obs�uga kolektora ************************************

if od.Execute then
begin
dm.csv.FileName := od.FileName;
dm.csv.Active := True;
dm.csv.First;

numer := numer + 1;
bledne := 1;
ProgressBar1.Min := 0;
ProgressBar1.Max := dm.csv.RecordCount;
ProgressBar1.StepBy(10);
y := 0;
automat := 'K';
dok_pierwotny := 'kolektor';
while not dm.csv.Eof do
      begin
           y := y + 1;
           ProgressBar1.Position := y;
           kod_kreskowy := dm.csv.Fields.Fields[0].AsString;
           cena := StrToFloat(StringReplace(dm.csv.Fields.Fields[2].AsString,'.',',',[rfReplaceAll]));
           ilosc := StrToFloat(StringReplace(dm.csv.Fields.Fields[1].AsString,'.',',',[rfReplaceAll]));
           dm.ZapiszCSV(numer,sklep,kod_kreskowy,cena,ilosc);
           nr_towar := dm.WyszukajTowary(kod_kreskowy,sklep);
           nazwa := dm.NazwaTowaru(nr_towar);
           wartosc := cena * ilosc;
           if nr_towar > 0 then
              dm.ZapiszArkusz(nr_towar,numer,sklep,ilosc,cena,wartosc,nazwa,automat,dok_pierwotny)
           else
               begin
                    dm.ZapiszTMP(numer,sklep,y,ilosc,cena,wartosc,kod_kreskowy);
                    sb.SimpleText := 'Znaleziono niezidentyfikowanych towar�w: ' + IntToStr(bledne);
                    bledne := bledne + 1;
                    pc.ActivePageIndex := 1;
                    UstawSiatke(bledne);
                    dm.PokazTMP;
                    dm.ibqTMP.First;
                    for x:=1 to bledne do
                        with dm.ibqTMP,Siatka do
                             begin
                                  if not FieldByName('NR_IMPORT').IsNull then Cells[1,x] := IntToStr(FieldValues['NR_IMPORT'])
                                     else Cells[1,x] := 'brak';
                                  if not FieldByName('NR_SKLEP').IsNull then Cells[2,x] := IntToStr(FieldValues['NR_SKLEP'])
                                     else Cells[2,x] := 'brak';
                                  if not FieldByName('TOWAR').IsNull then Cells[3,x] := FieldValues['TOWAR']
                                     else Cells[3,x] := 'brak';
                                  if not FieldByName('KOD_KRESKOWY').IsNull then Cells[4,x] := FieldValues['KOD_KRESKOWY']
                                     else Cells[4,x] := 'brak';
                                  if not FieldByName('ILOSC').IsNull then Cells[5,x] := FloatToStr(FieldValues['ILOSC'])
                                     else Cells[5,x] := 'brak';
                                  if not FieldByName('CENA').IsNull then Cells[6,x] := FloatToStr(FieldValues['CENA'])
                                     else Cells[6,x] := 'brak';
                                  if not FieldByName('WARTOSC').IsNull then Cells[7,x] := FloatToStr(FieldValues['WARTOSC'])
                                     else Cells[7,x] := 'brak';
                                  if not FieldByName('POZYCJA_PLIKU').IsNull then Cells[8,x] := FieldValues['POZYCJA_PLIKU']
                                     else Cells[8,x] := 'brak';
                                  Next;
                             end;
               end;
           dm.csv.Next;
      end;
dm.ZwiekszNr(numer);      
dm.PokazArkusz(numer);
dm.csv.Close;
end;
ProgressBar1.Position := 0;
pc.ActivePageIndex := 0;
end;

procedure TfrGlowny.Pokawczytany1Click(Sender: TObject);
begin
pc.ActivePageIndex := 3;
dm.PokazArkusze;
UstawSiatkeImport(DBGrid1);
end;

procedure TfrGlowny.DBGrid4CellClick(Column: TColumn);
var
   nr_importu: Integer;
   suma: Real;
begin
nr_importu := dm.ibqPokazArkusze.FieldValues['IMPORT'];
suma := dm.SumujPozycje(nr_importu);
sb.Panels.Items[0].Text := 'Warto�� pozycji na li�cie';
sb.Panels.Items[1].Alignment := taRightJustify;
sb.Panels.Items[1].Text := FloatToStrF(suma, ffCurrency, 7, 2);
dm.PokazPozycje(nr_importu,0);
//UstawSiatkeArkuszePozycje(DBGrid5);
UstawSiatkeArkuszePozycje1(XDBGrid1);
ToolButton3.Enabled := True;
JvRichEdit1.Text := 'Mo�esz edytowa� dowoln� pozycj� importu - kliknij na niej prawym klawiszem myszki';
dtpOd.Date := now;
dtpDo.Date := now;
jeNrOd.Text := '';
jeNrDo.Text := '';
end;

procedure TfrGlowny.owary1Click(Sender: TObject);
begin
frTowary.ShowModal;
end;

procedure TfrGlowny.Dodatkoweopcje1Click(Sender: TObject);
var
   haslo: String;
begin
haslo := InputBox('Podaj has�o do rozpocz�cia operacji', 'Podaj has�o', '');
if CompareStr(haslo, 'KompSerw086') = 0 then
   begin
        Pokaremament1.Visible := True;
        DrukujRemament1.Visible := True;
        ToolButton6.Visible := True;
        ToolButton4.Visible := True;
   end
else
    ShowMessage('B��dne has�o, podaj jeszcze raz');
end;

procedure TfrGlowny.Pokaremament1Click(Sender: TObject);
begin
//********************************** Poka� remament ****************************
end;

procedure TfrGlowny.DrukujRemament1Click(Sender: TObject);
var
   sklep: Integer;
begin
//********************************** Drukuj remament ****************************
frSklep.ShowModal;
if not dm.ibqSklepy.FieldByName('NR_SKLEPY').IsNull then sklep := dm.ibqSklepy.FieldValues['NR_SKLEPY']
   else
       begin
            ShowMessage('Brak sklepu, przerywam wydruk inwentaryzacji');
            Exit;
       end;
frxReport1.LoadFromFile('rem.fr3');
frxReport1.PrepareReport(True);
frxReport1.ShowReport();
end;

procedure TfrGlowny.TabSheet4Hide(Sender: TObject);
begin
sb.Panels.Items[0].Text := '';
sb.Panels.Items[1].Text := '';
end;

procedure TfrGlowny.TabSheet4Show(Sender: TObject);
begin
dm.PokazArkusze;
UstawSiatkeArkuszeLista(DBGrid4);
end;

procedure TfrGlowny.UstawSiatkeArkuszePozycje(aGrid: TDBGrid);
var
  lColumn: TColumn;
begin
with aGrid do
     begin
          Columns.Clear;
          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'NAZWA';
          lColumn.Title.Caption := 'Nazwa towaru';
          lColumn.Width := 300;

          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'ILOSC';
          lColumn.Title.Caption := 'Ilo�� / waga';
          lColumn.Width := 80;

          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'CENA';
          lColumn.Title.Caption := 'Cena sprzeda�y';
          lColumn.Width := 80;

          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'WARTOSC';
          lColumn.Title.Caption := 'Warto�� pozycji';
          lColumn.Width := 80;
     end;

end;

procedure TfrGlowny.DBGrid5DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
if (Column.Field.DataType In [ftFloat, ftCurrency, ftBCD])  and (Column.Field.FieldName = 'CENA') or (Column.Field.FieldName = 'WARTOSC') then
   (Column.Field AS TNumericField).DisplayFormat := '#,##0.00 z�';
if (Column.Field.DataType In [ftFloat, ftCurrency, ftBCD])  and (Column.Field.FieldName = 'ILOSC') then
   (Column.Field AS TNumericField).DisplayFormat := '#,##0.0000';
end;

procedure TfrGlowny.UstawSiatkeArkuszeLista(aGrid: TDBGrid);
var
  lColumn: TColumn;
begin
with aGrid do
     begin
          Columns.Clear;
          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'IMPORT';
          lColumn.Title.Caption := 'Nr importu';
          lColumn.Width := 60;

          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'DOKUMENT';
          lColumn.Title.Caption := 'Na podstawie';
          lColumn.Width := 80;

          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'NAZWA';
          lColumn.Title.Caption := 'Nazwa sklepu';
          lColumn.Width := 250;

          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'DATA';
          lColumn.Title.Caption := 'Data wycztania';
          lColumn.Width := 80;

          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'WARTOSC';
          lColumn.Title.Caption := 'Warto�� importu';
          lColumn.Width := 90;
     end;

end;

procedure TfrGlowny.DBGrid4DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
if (Column.Field.DataType In [ftFloat, ftCurrency, ftBCD])  and (Column.Field.FieldName = 'CENA') or (Column.Field.FieldName = 'WARTOSC') then
   (Column.Field AS TNumericField).DisplayFormat := '#,##0.00 z�';
end;

procedure TfrGlowny.UstawSiatkeImport(aGrid: TDBGrid);
var
  lColumn: TColumn;
begin
with aGrid do
     begin
          Columns.Clear;
          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'NAZWA';
          lColumn.Title.Caption := 'Nazwa towaru';
          lColumn.Width := 250;

          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'ILOSC';
          lColumn.Title.Caption := 'Ilo�� / waga';
          lColumn.Width := 80;

          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'CENA';
          lColumn.Title.Caption := 'Cena sprzeda�y';
          lColumn.Width := 80;

          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'WARTOSC';
          lColumn.Title.Caption := 'Warto��';
          lColumn.Width := 80;

     end;

end;

procedure TfrGlowny.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
if (Column.Field.DataType In [ftFloat, ftCurrency, ftBCD])  and (Column.Field.FieldName = 'CENA') or (Column.Field.FieldName = 'WARTOSC') then
   (Column.Field AS TNumericField).DisplayFormat := '#,##0.00 z�';
if (Column.Field.DataType In [ftFloat, ftCurrency, ftBCD])  and (Column.Field.FieldName = 'ILOSC') then
   (Column.Field AS TNumericField).DisplayFormat := '#,##0.0000';
end;

procedure TfrGlowny.dtpDoChange(Sender: TObject);
var
   od_data,do_data: TDateTime;
begin
od_data := dtpOd.Date;
do_data := dtpDo.Date;
dm.PokazArkusze(od_data,do_data);
UstawSiatkeArkuszeLista(DBGrid4);
end;

procedure TfrGlowny.jeNrDoChange(Sender: TObject);
var
   od_nr,do_nr: Integer;
begin
if (Length(jeNrOd.Text) > 0) or (Length(jeNrDo.Text) > 0) then
   begin
        od_nr := StrToInt(jeNrOd.Text);
        do_nr := StrToInt(jeNrDo.Text);
        dm.PokazArkusze(od_nr,do_nr);
        UstawSiatkeArkuszeLista(DBGrid4);

   end;
end;

procedure TfrGlowny.DBGrid5CellClick(Column: TColumn);
begin
sb.Font.Color := clRed;
sb.Panels[2].Text := 'Mo�esz edytowa� dowoln� pozycj� importu - kliknij na niej prawym klawiszem myszki';
end;

procedure TfrGlowny.Dodajpozycj1Click(Sender: TObject);
begin
//********************************************************Dodawanie pozycji do importu
frEdycjaPozycji.LabeledEdit1.Text := '';
frEdycjaPozycji.LabeledEdit2.Text := '';
frEdycjaPozycji.LabeledEdit3.Text := '';
frEdycjaPozycji.ShowModal;
end;

procedure TfrGlowny.Poprawpozycj1Click(Sender: TObject);
begin
//********************************************************Poprawianie pozycji importu
frEdycjaPozycji.LabeledEdit1.Text := dm.ibqPokazPozycje.FieldValues['NAZWA'];
frEdycjaPozycji.LabeledEdit2.Text := FloatToStr(dm.ibqPokazPozycje.FieldValues['CENA']);
frEdycjaPozycji.LabeledEdit3.Text := FloatToStr(dm.ibqPokazPozycje.FieldValues['ILOSC']);
frEdycjaPozycji.SetWyborAkcji(2);
frEdycjaPozycji.ShowModal;
end;

procedure TfrGlowny.Usupozycj1Click(Sender: TObject);
begin
//********************************************************Kasowanie pozycji importu
frEdycjaPozycji.LabeledEdit1.Text := dm.ibqPokazPozycje.FieldValues['NAZWA'];
frEdycjaPozycji.LabeledEdit2.Text := FloatToStr(dm.ibqPokazPozycje.FieldValues['CENA']);
frEdycjaPozycji.LabeledEdit3.Text := FloatToStr(dm.ibqPokazPozycje.FieldValues['ILOSC']);
frEdycjaPozycji.SetWyborAkcji(3);
frEdycjaPozycji.ShowModal;
end;

procedure TfrGlowny.Drukujtendokument1Click(Sender: TObject);
begin
//********************************************************Drukowanie importu

end;

procedure TfrGlowny.UstawSiatkeArkuszePozycje1(aGrid: TXDBGrid);
var
  lColumn: TColumn;
begin

with aGrid do
     begin
          {Columns.Clear;
          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.Title.Caption := 'Lp.';
          lColumn.Width := 40;   }
          Columns[0].AutoNumber := True;
          Columns[0].Title.Caption := 'Lp.';
          Columns[0].Width := 30;

          Columns[1].Visible := False;
          Columns[2].Visible := False;
          Columns[3].Visible := False;
          Columns[5].Visible := False;
          Columns[9].Visible := False;
          Columns[10].Visible := False;
          Columns[11].Visible := False;
          Columns[12].Visible := False;
          Columns[13].Visible := False;
          Columns[14].Visible := False;
          Columns[15].Visible := False;
          Columns[16].Visible := False;
          Columns[17].Visible := False;
          Columns[18].Visible := False;

          Columns[4].FieldName := 'NAZWA';
          Columns[4].Title.Caption := 'Nazwa towaru';
          Columns[4].Width := 300;

          Columns[6].FieldName := 'ILOSC';
          Columns[6].Title.Caption := 'Ilo��/waga';
          Columns[6].Width := 80;

          Columns[7].FieldName := 'CENA';
          Columns[7].Title.Caption := 'Cena sprzeda�y';
          Columns[7].Width := 80;

          Columns[8].FieldName := 'WARTOSC';
          Columns[8].Title.Caption := 'Warto�� pozycji';
          Columns[8].Width := 80;

          {lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'NAZWA';
          lColumn.Title.Caption := 'Nazwa towaru';
          lColumn.Width := 300;

          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'ILOSC';
          lColumn.Title.Caption := 'Ilo�� / waga';
          lColumn.Width := 80;

          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'CENA';
          lColumn.Title.Caption := 'Cena sprzeda�y';
          lColumn.Width := 80;

          lColumn := Tcolumn.Create(aGrid.columns);
          lColumn.FieldName := 'WARTOSC';
          lColumn.Title.Caption := 'Warto�� pozycji';
          lColumn.Width := 80; }
     end;

end;

end.


