unit tresc_pliku;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, JvExControls, JvLabel, ExtCtrls, JvExExtCtrls, JvExtComponent,
  JvPanel, JvEditorCommon, JvEditor, StdCtrls, JvExStdCtrls, JvButton,
  JvCtrls;

type
  TfrPokazTrescPliku = class(TForm)
    JvEditor1: TJvEditor;
    JvPanel1: TJvPanel;
    JvLabel1: TJvLabel;
    LabeledEdit1: TLabeledEdit;
    LabeledEdit2: TLabeledEdit;
    LabeledEdit3: TLabeledEdit;
    JvImgBtn1: TJvImgBtn;
    procedure UstawPlik(plik: String);
    procedure WyswietlPlik;
    procedure JvImgBtn1Click(Sender: TObject);
    function UsunZaDuzo(S: String): String;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frPokazTrescPliku: TfrPokazTrescPliku;
  plik_nazwa: String;

implementation

uses baza,glowny, towary;

{$R *.dfm}

{ TfrPokazTrescPliku }

procedure TfrPokazTrescPliku.UstawPlik(plik: String);
begin
     plik_nazwa := plik;
end;

procedure TfrPokazTrescPliku.WyswietlPlik;
var
  TF : TextFile;
  S : String;
begin
     AssignFile(TF, plik_nazwa);
     Reset(TF);
     while not Eof(TF) do
           begin
                Readln(TF,S);
                //S := UsunZaDuzo(S);
                JvEditor1.Lines.Add(S);
           end;
     CloseFile(TF);
end;

procedure TfrPokazTrescPliku.JvImgBtn1Click(Sender: TObject);
begin
frTowary.UstawPole1(StrToInt(LabeledEdit1.Text));
frTowary.UstawPole2(StrToInt(LabeledEdit2.Text));
frTowary.UstawPole3(StrToInt(LabeledEdit3.Text));
Close;
end;

function TfrPokazTrescPliku.UsunZaDuzo(S: String): String;
begin
//
end;

end.
