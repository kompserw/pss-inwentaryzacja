program Inwentaryzacja;

uses
  Forms,
  glowny in 'glowny.pas' {frGlowny},
  baza in 'baza.pas' {dm: TDataModule},
  sklep in 'sklep.pas' {frSklep},
  towary in 'towary.pas' {frTowary},
  dodaj_towar in 'dodaj_towar.pas' {frDodajTowar},
  tresc_pliku in 'tresc_pliku.pas' {frPokazTrescPliku},
  edycja_pozycji in 'edycja_pozycji.pas' {frEdycjaPozycji};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrGlowny, frGlowny);
  Application.CreateForm(Tdm, dm);
  Application.CreateForm(TfrSklep, frSklep);
  Application.CreateForm(TfrTowary, frTowary);
  Application.CreateForm(TfrDodajTowar, frDodajTowar);
  Application.CreateForm(TfrPokazTrescPliku, frPokazTrescPliku);
  Application.CreateForm(TfrEdycjaPozycji, frEdycjaPozycji);
  Application.Run;
end.
