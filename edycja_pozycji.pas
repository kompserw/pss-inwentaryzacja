unit edycja_pozycji;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, JvExStdCtrls, JvButton, JvCtrls;

type
  TfrEdycjaPozycji = class(TForm)
    LabeledEdit1: TLabeledEdit;
    LabeledEdit2: TLabeledEdit;
    LabeledEdit3: TLabeledEdit;
    JvImgBtn1: TJvImgBtn;
    JvImgBtn2: TJvImgBtn;
    procedure JvImgBtn2Click(Sender: TObject);
    procedure JvImgBtn1Click(Sender: TObject);
    procedure SetWyborAkcji(wybor: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frEdycjaPozycji: TfrEdycjaPozycji;
  WyborAkcji: Integer;

implementation

uses glowny,baza;
{$R *.dfm}

procedure TfrEdycjaPozycji.JvImgBtn2Click(Sender: TObject);
begin
LabeledEdit1.Text := '';
LabeledEdit2.Text := '';
LabeledEdit3.Text := '';
Close;
end;

procedure TfrEdycjaPozycji.JvImgBtn1Click(Sender: TObject);
var
   nr_towaru,nr_import,nr_sklep: Integer;
   ilosc,cena,wartosc: Real;
   nazwa,typ,dok_pierwotny: String;
begin
nr_towaru := dm.ibqPokazPozycje.FieldValues['NR_TOWAR'];
nr_import := dm.ibqPokazPozycje.FieldValues['NR_IMPORT'];
nr_sklep := dm.ibqPokazPozycje.FieldValues['NR_SKLEP'];
dok_pierwotny := dm.ibqPokazPozycje.FieldValues['NAZWA_DOK_PIERW'];
nazwa := LabeledEdit1.Text;
ilosc := StrToFloat(LabeledEdit2.Text);
cena := StrToFloat(LabeledEdit3.Text);
wartosc := ilosc * cena;
typ := 'R';
case WyborAkcji of
     2: dm.PoprawArkusz(nr_towaru,nr_import,nr_sklep,ilosc,cena,wartosc,nazwa,typ,dok_pierwotny);
     3: dm.KasujPozycjeArkusz(nr_towaru,nr_import,nr_sklep);
end;
dm.PokazArkusze;
dm.PokazPozycje(nr_import,0);
frGlowny.UstawSiatkeArkuszeLista(frGlowny.DBGrid4);
frGlowny.UstawSiatkeArkuszePozycje1(frGlowny.XDBGrid1);
LabeledEdit1.Text := '';
LabeledEdit2.Text := '';
LabeledEdit3.Text := '';
Close;
end;

procedure TfrEdycjaPozycji.SetWyborAkcji(wybor: Integer);
begin
WyborAkcji := wybor;
end;

end.
